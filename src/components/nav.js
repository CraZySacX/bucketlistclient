import React from 'react';
import { MenuItem, Nav, Navbar, NavDropdown, NavItem } from 'react-bootstrap';

const NavBarHeader = () => (
    <Navbar>
        <Navbar.Header>
            <Navbar.Brand>
                <a href="#">Bucket List</a>
            </Navbar.Brand>
        </Navbar.Header>
        <Nav>
            <NavItem eventKey={1} href="#">Sign In</NavItem>
            <NavItem eventKey={1} href="#">Sign Up</NavItem>
            <NavDropdown eventKey={3} title="Cool Stuff" id="bl-nav-dropdown">
                <MenuItem eventKey={3.1}>Action 1</MenuItem>
                <MenuItem eventKey={3.2}>Action 2</MenuItem>
            </NavDropdown>
        </Nav>
    </Navbar>
);

export default NavBarHeader;