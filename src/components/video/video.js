import _ from 'lodash';
import React from 'react';
import YTSearch from 'youtube-api-search';
import SearchBar from './search-bar';
import VideoDetail from './video-detail';

const API_KEY = 'AIzaSyDPB7eSpQPecvGXPxQ1ghFsJlE_FjCstio';

class Video extends React.Component {
    state = {
        videos: [],
        selectedVideo: null
    };

    constructor(props) {
        super(props)
        this.videoSearch('bucketlist');
    }

    videoSearch = (term) => {
        YTSearch({key: API_KEY, term: term}, (videos) => {
            this.setState({
                videos: videos,
                selectedVideo: videos[0]
            });
        });
    }

    render() {
        const videoSearch = _.debounce((term) => { this.videoSearch(term); }, 300);
        return(
            <div>
                <SearchBar onSearchTermChange={videoSearch} />
                <VideoDetail video={this.state.selectedVideo} />
            </div>
        );
    }
}

export default Video;