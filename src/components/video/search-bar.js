import React from 'react';
import PropTypes from 'prop-types';

class SearchBar extends React.Component {
    static propTypes = {
        onSearchTermChange: PropTypes.func.isRequired,
    };

    state = {
        term: 'bucketlist'
    };

    constructor(props) {
        super(props)
    }

    render() {
        return(
            <div className="search-bar">
                <label className="vidSearchLbl">Get Inspired!</label>
                <input value={this.state.term} onChange={ (event) => this.onInputChange(event.target.value) } />
            </div>
        );
    }

    onInputChange = (term) => {
        this.setState({term});
        this.props.onSearchTermChange(term);
    }
}

export default SearchBar;