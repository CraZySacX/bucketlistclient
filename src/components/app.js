import React from 'react';
import NavBarHeader from './nav';
import Video from './video/video';

const App = () => (
    <div>
        <NavBarHeader />
        <Video />
    </div>
);

export default App;