import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import {
    BrowserRouter as Router,
    Route
} from 'react-router-dom';

import App from './components/app';
import reducers from './reducers';

var createStoreWithMiddleware = applyMiddleware()(createStore);

ReactDOM.render(
    <Provider store={createStoreWithMiddleware(reducers)}>
        <Router>
            <Route exact path="/" component={App}></Route>
        </Router>
    </Provider>, document.querySelector('.container'));